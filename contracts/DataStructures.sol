pragma solidity 0.4.24;


contract DataStructures {

    // Structs define a data object 
    struct BlockchainCoder {
        string name; 
        uint karma;
    }

    // Array that holds BlockchainCoder Structs 
    BlockchainCoder[] public coders; 
    // Mapping to keep a reference of an address to the coders array index
    mapping(address => uint) public addrToCoderIndex; 

    function addNewCoder(string _name, uint _karma) external {
        // Push returns the new length of the array 
        uint index = coders.push(BlockchainCoder(_name, _karma)) - 1;
        // Set a reference in the mapping to the newly created Coder 
        addrToCoderIndex[msg.sender] = index; 
    }

    function getMyDetails() external view returns(string _name, uint _karma) {
        // Find the index of the sender
        uint coderIndex = addrToCoderIndex[msg.sender];
        // Retrieve the coder from the coders array
        BlockchainCoder memory coder = coders[coderIndex];
        // Structs cannot be returned, but Solidity supports multiple return values 
        return(coder.name, coder.karma);
    }
}