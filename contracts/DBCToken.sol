pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

contract DBCToken is ERC20 {
    // Public variables have automatic getters created, but not setters.
    string public name = "DallasBlockchainCoders";
    string public symbol = "DBC";
    uint public decimals = 18;
    uint public INITIAL_SUPPLY = 100000 * (10**decimals);
    
    // Called only once during deployment
    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }
}

