// npm install truffle-hdwallet-provider
const HDWalletProvider = require("truffle-hdwallet-provider");
// Store environment-specific variable from '.env' to process.env
require('dotenv').config();


module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  // truffle migrate 
  networks: {
    development: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "*", // Match any network id
    },
    // truffle migrate --network ganacheGUI
    ganacheGUI: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*", // Match any network id
    },
    // truffle migrate --network ropsten
    ropsten: {
      provider: () => new HDWalletProvider(
        process.env.MNEMONIC, 
        "https://ropsten.infura.io/v3/" + process.env.INFURA_API_KEY, 
        0 // <- You can specifiy which account to use with the optional third variable 
      ), 
      network_id: 3,
      gas: 3000000, // Optional
      gasPrice: 21 // Optional
    }

  }
};
