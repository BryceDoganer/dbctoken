# DBCToken

An example ERC20 token project for Dallas Blockchain Coders.

**Link to the [Solidity Presentation.](https://docs.google.com/presentation/d/17ldhH_77_9t9s4lTzRDVXbNbcs99sQELZVGCw6QYE2I/edit?usp=sharing)**

#### If you don't have Truffle installed:
`npm install -g truffle`

## Project Setup
1. Clone repository locally
2. `cd` into project directory
3. run `npm install` to install node_modules

## Setup `dotenv` to deploy to testnet/mainnet
1. Create a file named `.env` in the root project directory
2. Setup environment variables in `.env` to connect to Infura as follows:  
`MNEMONIC=<your-ethereum-wallet-seed-phrase>`  
`INFURA_API_KEY=<your-infura-api-key>`

## Compile and Migrate 
`truffle compile`  
`truffle migrate` - For Truffle development blockchain  
`truffle migrate --network ganacheGUI` - For Truffle's GanacheGUI development blockchain  
`truffle migrate --network ropsten` - For Ropsten testnet   